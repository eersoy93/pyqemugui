# Copyright (c) 2023 Erdem Ersoy (eersoy93)
#
# Licensed with MIT license. See LICENSE file for full text.

import PySimpleGUI as psg
import subprocess
import os
import sys

EXIT_SUCCESS = 0
EXIT_FAILURE = 1
VERSION = "0.0.1"


def main(args):
    psg.theme("Default1")

    layout = [[psg.Text("Location of the QEMU executable:", size=(30, 1)), psg.InputText(), psg.FileBrowse()],
              [psg.Text("CD-ROM image file location:", size=(30, 1)), psg.InputText(), psg.FileBrowse()],
              [psg.Text("Memory size (MB):", size=(30, 1)), psg.InputText()],
              [psg.Text("Other command line options:", size=(30, 1)), psg.InputText()],
              [psg.Button("Run"), psg.Button("Quit")]]

    window = psg.Window(f"PyQemuGUI {VERSION}", layout, size=(665, 165))

    while True:
        event, values = window.read()
        if event == psg.WIN_CLOSED or event == "Quit":
            return EXIT_SUCCESS
        elif event == "Run":
            qemu_location = values[0]
            image_file = values[1]
            memory_size = values[2]
            other_opts = values[3]

            if not os.path.exists(qemu_location):
                psg.popup(f"QEMU executable not found at {qemu_location}. Please provide a valid location.")
                continue
            elif not os.path.exists(image_file):
                psg.popup(f"Image file not found at {image_file}. Please provide a valid location.")
                continue

            try:
                memory_size_int = int(memory_size)
                if memory_size_int <= 0:
                    raise ValueError
            except ValueError:
                psg.popup(f"Memory size is invalid. Please provide a valid size.")
                continue

            cli_string = f'"{qemu_location}" -m {memory_size} -cdrom "{image_file}" {other_opts}'
            subprocess.Popen(cli_string, shell=True)


if __name__ == "__main__":
    sys.exit(main(sys.argv))

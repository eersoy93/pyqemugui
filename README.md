# pyQemuGUI
A very simple QEMU GUI frontend written with PySimpleGUI.

## Author
Erdem Ersoy (eersoy93) (with the help of ChatGPT)

## Copyright
Copyright (c) 2023 Erdem Ersoy (eersoy93)

## License
Licensed with MIT license. See LICENSE for license text.

